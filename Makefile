# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

clean:
	rm -rf ./build ./django_secret_settings.egg-info ./dist ./.coverage ./htmlcov

coverage:
	pip install -r requirements-dev.txt
	python3 -m coverage run --source django_secret_settings -m unittest discover
	python3 -m coverage html
	python3 -m coverage report

init:
	python3 -m venv ./venv
	. venv/bin/activate
	pip install --upgrade pip
	pip install -r requirements-dev.txt

integrationtest:
	python3 -m unittest discover -s tests/integrationtests/

test: unittest integrationtest

unittest:
	python3 -m unittest discover -s tests/unittests/

.PHONY: clean coverage init integrationtest unittest
